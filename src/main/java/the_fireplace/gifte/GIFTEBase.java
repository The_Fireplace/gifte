package the_fireplace.gifte;

import the_fireplace.gifte.blocks.*;
import the_fireplace.gifte.items.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid="gifte", name="Griefing Is Far Too Easy", version="pre1.0")
public class GIFTEBase {
@Instance("gifte")
public static GIFTEBase instance;

public static Block TitaniumOre = new TitaniumOre(Material.rock);
public static Block TitaniumBlock = new TitaniumBlock(Material.iron);
public static Block TitaniumTile = new TitaniumTile(Material.iron);
public static Item TitaniumIngot = new TitaniumIngot();
public static Item TitaniumDust = new TitaniumDust();
public static Item BlackLightDust = new BlackLightDust();
public static Item SoulSandDust = new SoulSandDust();
public static Item ChargedDust = new ChargedDust();

@EventHandler
public static void PreInit(FMLPreInitializationEvent event){
	GameRegistry.registerBlock(TitaniumBlock, "TitaniumBlock");
	GameRegistry.registerBlock(TitaniumTile, "TitaniumTile");
	GameRegistry.registerBlock(TitaniumOre, "TitaniumOre");
	GameRegistry.registerItem(TitaniumIngot, "TitaniumIngot");
	GameRegistry.registerItem(TitaniumDust, "TitaniumDust");
	GameRegistry.registerItem(BlackLightDust, "BlackLightDust");
	GameRegistry.registerItem(SoulSandDust, "SoulSandDust");
	GameRegistry.registerItem(ChargedDust, "ChargedDust");
	OreDictionary.registerOre("oreTitanium", TitaniumOre);
	OreDictionary.registerOre("blockTitanium", TitaniumBlock);
	OreDictionary.registerOre("ingotTitanium", TitaniumIngot);
	OreDictionary.registerOre("dustTitanium", TitaniumDust);
}
@EventHandler
public static void Init(FMLInitializationEvent event){
	ItemStack TitaniumBlockStack = new ItemStack(TitaniumBlock);
	ItemStack TitaniumTileStack = new ItemStack(TitaniumTile);
	ItemStack TitaniumIngotStack = new ItemStack(TitaniumIngot);
	ItemStack TitaniumIngotStack9 = new ItemStack(TitaniumIngot, 9);
	ItemStack TitaniumOreStack = new ItemStack(TitaniumOre);
	ItemStack TitaniumDustStack = new ItemStack(TitaniumDust);
	GameRegistry.addRecipe(TitaniumBlockStack, "ttt", "ttt", "ttt", 't', TitaniumIngotStack);
	GameRegistry.addShapelessRecipe(TitaniumIngotStack9, TitaniumBlockStack);
	GameRegistry.addSmelting(TitaniumIngotStack, TitaniumDustStack, 2);
	GameRegistry.addSmelting(TitaniumIngotStack, TitaniumOreStack, 5);
}
}
