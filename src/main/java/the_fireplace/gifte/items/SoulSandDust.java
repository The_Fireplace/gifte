package the_fireplace.gifte.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class SoulSandDust extends Item {
	public SoulSandDust(){
		setTextureName("gifte:soulsand_dust");
		setUnlocalizedName("SoulSandDust");
		setCreativeTab(CreativeTabs.tabMaterials);
	}

}
