package the_fireplace.gifte.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class TitaniumOre extends Block {

	public TitaniumOre(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockTextureName("gifte:titanium_ore");
		setBlockName("TitaniumOre");
		setCreativeTab(CreativeTabs.tabBlock);
        setHardness(25.0F);
	}

}