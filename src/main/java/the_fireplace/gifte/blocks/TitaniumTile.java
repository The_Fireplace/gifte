package the_fireplace.gifte.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class TitaniumTile extends Block {

	public TitaniumTile(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockTextureName("gifte:titanium_tile");
		setBlockName("TitaniumTile");
		setCreativeTab(CreativeTabs.tabBlock);
	}

}
